# Server Provisioning and Configuration Playbook

* Introduction
* Playbook Roles
* Ansible Role Variables
* Manual Execution of the Playbook

## Introduction

This Playbook was designed for the creation and configuration of AWS instances/VMs. The Playbook provides the ability to spin up a instance on a specified network with the ability to configure several standard configuration options for each system outlined in the *Playbook Options* section.


## Playbook Roles
* instance_provisioning:
This role provision the new instance on given AWS account. The required instance configuration is specified in `vars\main.yml` config file.
* configure_instance:
This inlcudes the tasks used to setup the software components, custom packages and update the configuration files etc on remote server.

## Ansible Role Variables
* `/roles/instance_provisioning/vars/main.yml`
This inlcudes config details used by aws while creating the instance like `instance name`, `security group`, `private key`  etc.
* `/roles/instance_provisioning/vars/ec2_creds.yml`
This holds the `aws_access_key` and `aws_secret_key` used to access the AWS account in encrypted format.
    
## Manual Execution of the Playbook

If you would like to run this Playbook from an Ansible Workstation you can follow these steps.

1. You'll need to clone or copy the repo to a location on the system.
    * `$ git clone repo_name`

2. In order to get it to run you'll need to modify the variables related to AWS
    * You'll need to edit */roles/instance_provisioning/vars/main.yml* 
    * You'll need to edit */roles/instance_provisioning/vars/ec2_creds.yml* 
3. You'll need a file that contains the vault password, in the example below a file *vault_pass.txt* contains the password for the vaults and is stored in your home folder.
4. Run the `ansible-vault encrypt` command to encrypt the `ec2_creds.yml`
5. Run the playbook using: 
    * `ansible-playbook -i hosts deploy.yml --vault-password-file ~/vault_pass.txt`

### Useful commands:
```ansibleCLI
# Decrypt
ansible-vault decrypt --vault-password-file <pathToFile>

# Edit
# You can modify it with any text editor or use something like this
echo "PasswordHere" >> <pathToFile>

# Encrypt
ansible-vault encrypt --vault-password-file <pathToFile>

# View
ansible-vault view --vault-password-file <pathToFile>

```